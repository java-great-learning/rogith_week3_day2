package com.basics.week3.day2;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Shop {

	int num;
	boolean flag = false;



	public Shop() {

	}
	public synchronized void produce(int num)
	{
		while(flag)
		{
			try {
				System.out.println(Thread.currentThread().getName()+" is waiting");
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		this.num = num;
		flag = true;
		notify();

	}

	public synchronized int get()
	{
		while(!flag)
		{
			try {
				System.out.println(Thread.currentThread().getName()+" is waiting");
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		flag = false;
		notify();
		return num;
	}



	Lock l = new ReentrantLock();
	Condition isfull = l.newCondition();
	Condition isempty = l.newCondition();
	//	public  void produce(int num)
	//	{
	//		try{
	//			l.lock();
	//		while(flag)
	//		{	
	//			try {
	//				System.out.println(Thread.currentThread().getName()+" is waiting");
	//				isfull.await();
	//			} catch (InterruptedException e) {
	//				// TODO Auto-generated catch block
	//				e.printStackTrace();
	//			}
	//			
	//		}
	//		this.num = num;
	//		flag = true;
	//		isempty.signal();
	//		}
	//		finally
	//		{
	//			l.unlock();
	//		}
	//		
	//	}
	//	
	//	public int get()
	//	{
	//		try
	//		{
	//			l.lock();
	//			while(!flag)
	//			{
	//			try {
	//				System.out.println(Thread.currentThread().getName()+" is waiting");
	//				isempty.await();
	//			} catch (InterruptedException e) {
	//				// TODO Auto-generated catch block
	//				e.printStackTrace();
	//			}
	//		}
	//		flag = false;
	//		isfull.signal();
	//		return num;
	//		}
	//		finally
	//		{
	//			l.unlock();
	//			
	//		}
	//	}
}

