package LabQuestions;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class ProduceConsumerDemo {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		BlockingQueue<String> b = new LinkedBlockingQueue<String>(5);
		Thread producerThread =  new Thread(new ProducerThread(b));
		Thread consumerThread =  new Thread(new ConsumerThread(b));
		producerThread.start();
		consumerThread.start();
		producerThread.join();
		consumerThread.join();
	}

}
