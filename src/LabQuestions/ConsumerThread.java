package LabQuestions;

import java.util.concurrent.BlockingQueue;

public class ConsumerThread implements Runnable{
	
	private String data[] = {"Hi","Hello","Pop","Push","Love"};
	private BlockingQueue<String> b;
	
	public ConsumerThread(BlockingQueue<String> b)
	{
		this.b = b;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
		while(true)
		{
			String i=b.take();
			
			System.out.println("Consumer Consumes ["+i+"]");
		}
		}catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
}
