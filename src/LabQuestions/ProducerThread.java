package LabQuestions;

import java.util.concurrent.BlockingQueue;

//Question 1. 

//Create a class Producer that inserts Strings into the blocking-queue. It sleeps for 1000ms between each put call. (This is to cause the Consumer to block, while waiting for objects in the queue.)

//Create a class Consumer that takes out Strings from the same queue and prints them to console.

//Create a class Main with main method. It will start Producer and Consumer in separate threads. Create a BlockingQueue object that will be shared by a Producer (to insert Strings) and a Consumer object (to take out and print Strings)

public class ProducerThread implements Runnable{
	
	private String data[] = {"Hi","Hello","Pop","Push","Love"};
	private BlockingQueue<String> b;
	
	public ProducerThread(BlockingQueue<String> b)
	{
		this.b = b;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
		int i=0;
		while(true)
		{
			b.put(data[i]);
			System.out.println("Producer Produces["+data[i]+"]");
			Thread.sleep(1000);
			i++;
		}
		}catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

}
